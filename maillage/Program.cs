﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace geom
{
    class Program
    {
        static void Main(string[] args)
        {
            Mesh test = GestionFichierOff.LireFichierOff(@"..\..\mesh\triceratops.off");
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\copy-triceratops.off", test);

            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\cube.off", new Cube(2).Mesh());

            Console.Write(new Cube(2).Mesh().Proprietes());
            Console.ReadLine();

            Cylindre cylindre = new Cylindre(2, 5);
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\cylindre1.off", cylindre.Mesh(6));
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\cylindre2.off", cylindre.Mesh(20));

            Console.Write(cylindre.Mesh(6).Proprietes());
            Console.ReadLine();

            Sphere sphere = new Sphere(5);
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\sphere1.off", sphere.Mesh(8, 4));
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\sphere2.off", sphere.Mesh(20, 8));

            Console.Write(sphere.Mesh(8, 4).Proprietes());
            Console.ReadLine();

            Cone cone = new Cone(2, 4, 3);
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\cone1.off", cone.Mesh(6));
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\cone2.off", cone.Mesh(12));

            Cone cone2 = new Cone(2, 4, -1);
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\cone3.off", cone2.Mesh(6));
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\cone4.off", cone2.Mesh(12));

            Pacman pacman1 = new Pacman(5, 0.75);
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\pacman1.off", pacman1.Mesh(12, 8));

            Pacman pacman2 = new Pacman(5, 0.90);
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\pacman2.off", pacman2.Mesh(20, 6));
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\pacmanNormalized.off", pacman2.Mesh(20, 6).Normalisation());

            List<Point> ligne = new List<Point>();
            ligne.Add(new Point(0, 0, 0));
            ligne.Add(new Point(1, 1, 1));
            ligne.Add(new Point(0, 0, 2));
            ligne.Add(new Point(-0.5, -0.5, 3));
            ligne.Add(new Point(-0.8, -0.8, 4));
            ligne.Add(new Point(-0.2, -0.2, 5));
            LigneCylindrique ligneCylindrique = new LigneCylindrique(0.2, ligne);
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\ligneCylindrique.off", ligneCylindrique.Mesh(20));

            TubeCoude tubeCoude = new TubeCoude(0.2, ligne);
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\tubeCoude.off", tubeCoude.Mesh(20));
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\tubeCoudeCentre.off", tubeCoude.Mesh(20).Centrer());
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\tubeCoudeNormalise.off", tubeCoude.Mesh(20).Normalisation().Centrer());

            Mesh triceratops = GestionFichierOff.LireFichierOff(@"..\..\mesh\triceratops.off");
            GestionFichierOff.EcrireFichierOff(@"..\..\mesh\triceratopsNormalise.off", triceratops.Normalisation().Centrer());
        }
    }
}
