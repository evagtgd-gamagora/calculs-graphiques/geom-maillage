﻿using System;
using System.Collections.Generic;

namespace geom
{
    public class Mesh
    {

        public List<Point> sommets;
        public List<Facette> facettes;
        public Point centreG;

        public Mesh(List<Point> sommets, List<Facette> facettes)
        {
            this.sommets = sommets;
            this.facettes = facettes;
            centreG = CentreGravite();
        }

        private Point CentreGravite()
        {
            Point centre = new Point(0, 0, 0);

            foreach (Point s in sommets)
                centre += s;

            centre /= sommets.Count;
            return centre;
        }

        public Mesh Centrer()
        {

            for (int i = 0; i < sommets.Count; ++i)
            {
                sommets[i].x -= centreG.x;
                sommets[i].y -= centreG.y;
                sommets[i].z -= centreG.z;
            }
                
            centreG = CentreGravite();

            return this;
        }

        public Mesh Normalisation()
        {
            double dMax = 0;

            foreach (Point s in sommets)
            {
                Point p = s - centreG;
                double d = p.LongueurAuCarre();
                if (d > dMax)
                    dMax = d;
                //if (Math.Abs(p.x) > dMax)
                //    dMax = Math.Abs(p.x);
                //if (Math.Abs(p.y) > dMax)
                //    dMax = Math.Abs(p.y);
                //if (Math.Abs(p.z) > dMax)
                //    dMax = Math.Abs(p.z);
            }

            dMax = Math.Sqrt(dMax);

            for (int i = 0; i < sommets.Count; ++i)
            {
                Point direction = sommets[i] - centreG;
                direction /= dMax;

                sommets[i].x = direction.x + centreG.x;
                sommets[i].y = direction.y + centreG.y;
                sommets[i].z = direction.z + centreG.z;
            }

            return this;
        }

        public String Proprietes()
        {
            String proprietes = "";

            proprietes += "Nombres de sommets : " + sommets.Count + "\n";
            proprietes += "Nombres de facettes : " + facettes.Count + "\n";

            Dictionary<Point, List<Arete>> mapPointArete = new Dictionary<Point, List<Arete>>();

            foreach(Point s in sommets)
            {
                mapPointArete.Add(s, new List<Arete>());
            }

            List<Arete> aretes = new List<Arete>();
            Dictionary<Arete, List<Facette>> mapAreteFacette = new Dictionary<Arete, List<Facette>>();

            foreach (Facette facette in facettes)
            {
                Arete a1 = new Arete(facette.p1, facette.p2);
                Arete a2 = new Arete(facette.p2, facette.p3);
                Arete a3 = new Arete(facette.p3, facette.p1);

                //Aretes
                if (!aretes.Contains(a1))
                    aretes.Add(a1);
                if (!aretes.Contains(a2))
                    aretes.Add(a2);
                if (!aretes.Contains(a3))
                    aretes.Add(a3);

                //Aretes par point
                if (!mapPointArete[facette.p1].Contains(a1))
                    mapPointArete[facette.p1].Add(a1);
                if (!mapPointArete[facette.p1].Contains(a3))
                    mapPointArete[facette.p1].Add(a3);
                if (!mapPointArete[facette.p2].Contains(a1))
                    mapPointArete[facette.p2].Add(a1);
                if (!mapPointArete[facette.p2].Contains(a2))
                    mapPointArete[facette.p2].Add(a2);
                if (!mapPointArete[facette.p3].Contains(a2))
                    mapPointArete[facette.p3].Add(a2);
                if (!mapPointArete[facette.p3].Contains(a3))
                    mapPointArete[facette.p3].Add(a3);

                //Facette par arete init
                if (!mapAreteFacette.ContainsKey(a1))
                    mapAreteFacette.Add(a1, new List<Facette>());
                if (!mapAreteFacette.ContainsKey(a2))
                    mapAreteFacette.Add(a2, new List<Facette>());
                if (!mapAreteFacette.ContainsKey(a3))
                    mapAreteFacette.Add(a3, new List<Facette>());

                //Facette par arete
                mapAreteFacette[a1].Add(facette);
                mapAreteFacette[a2].Add(facette);
                mapAreteFacette[a3].Add(facette);
            }

            int minAretesParSommet = aretes.Count;
            int maxAretesParSommet = 0;
            

            foreach (List<Arete> l in mapPointArete.Values)
            {
                int n = l.Count;
                minAretesParSommet = Math.Min(minAretesParSommet, n);
                maxAretesParSommet = Math.Max(maxAretesParSommet, n);
            }

            int nAretesUneFacette = 0;
            foreach (List<Facette> l in mapAreteFacette.Values)
                if (l.Count == 1)
                    nAretesUneFacette++;

            proprietes += "Nombres d'arêtes : " + aretes.Count + "\n";
            proprietes += "Nombres d'arêtes par facettes : " + Facette.nSommets + "\n";
            proprietes += "Nombres d'arêtes par sommets min : " + minAretesParSommet + "\n";
            proprietes += "Nombres d'arêtes par sommets max : " + maxAretesParSommet + "\n";
            proprietes += "Nombres d'arêtes partagées par 1 seule facette : " + nAretesUneFacette + "\n";

            return proprietes;
        }
    }

    public class Point
    {
        public double x, y, z;

        public Point(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public double Longueur()
        {
            return Math.Sqrt(LongueurAuCarre());
        }

        public double LongueurAuCarre()
        {
            return Math.Pow(x, 2) + Math.Pow(y, 2) + Math.Pow(z, 2);
        }

        public Point Normaliser()
        {
            double l = Longueur();
            x /= l;
            y /= l;
            z /= l;
            return this;
        }

        public static Point Vectoriel(Point a, Point b)
        {
            if ((a == null) || (b == null))
                return null;

            Point vectoriel = new Point(
                a.y * b.z - a.z * b.y,
                a.z * b.x - a.x * b.z,
                a.x * b.y - a.y * b.x
                );

            return vectoriel;
        }

        public static Point operator +(Point a, Point b)
        {
            if ((a == null) || (b == null))
                return null;
            return new Point(a.x + b.x, a.y + b.y, a.z + b.z);
        }

        public static Point operator -(Point a, Point b)
        {
            if ((a == null) || (b == null))
                return null;
            return new Point(a.x - b.x, a.y - b.y, a.z - b.z);
        }

        public static Point operator *(Point p, double d)
        {
            if (p == null)
                return null;
            p.x *= d;
            p.y *= d;
            p.z *= d;
            return p;
        }

        public static Point operator /(Point p, double d)
        {
            if ((p == null) || (d == 0))
                return null;
            p.x /= d;
            p.y /= d;
            p.z /= d;
            return p;
        }

        public override bool Equals(Object o)
        {
            var item = o as Point;
            if (item == null)
                return false;
            
            return (x == item.x) && (y == item.y) && (z == item.z);
        }
    }

    public class Facette
    {
        public const bool sensTrigo = true;
        public const int nSommets = 3;
        public Point p1, p2, p3;

        public Facette(Point p1, Point p2, Point p3)
        {
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
        }

        public Point Normale()
        {
            Point normale = new Point(0, 0, 0);
            normale = Point.Vectoriel(p2 - p1, p3 - p2);
            return normale;
        }
    }

    public class Arete
    {
        public Point p1, p2;

        public Arete (Point p1, Point p2)
        {
            this.p1 = p1;
            this.p2 = p2;
        }

        public override bool Equals(Object o)
        {
            var item = o as Arete;
            if (item == null)
                return false;

            return (p1 == item.p1) && (p2 == item.p2)
                || (p1 == item.p2) && (p2 == item.p1);
        }

        public override int GetHashCode()
        {
            return p1.GetHashCode() + p2.GetHashCode();
        }
    }
}