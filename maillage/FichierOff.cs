﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace geom
{
    static class GestionFichierOff
    {

        public static void EcrireFichierOff(string nomFichier, Mesh mesh)
        {
            string textFichier = "";

            textFichier += "OFF\n";
            textFichier += mesh.sommets.Count + " " + mesh.facettes.Count + " 0\n";

            foreach (Point point in mesh.sommets)
            {
                textFichier += point.x.ToString(System.Globalization.CultureInfo.InvariantCulture) 
                    + " " + point.y.ToString(System.Globalization.CultureInfo.InvariantCulture) 
                    + " " + point.z.ToString(System.Globalization.CultureInfo.InvariantCulture) + "\n";
            }
                

            foreach (Facette facette in mesh.facettes)
                textFichier += Facette.nSommets + " " + mesh.sommets.IndexOf(facette.p1) + " " + mesh.sommets.IndexOf(facette.p2) + " " + mesh.sommets.IndexOf(facette.p3) + "\n";


            File.WriteAllText(nomFichier, textFichier);
        }


        public static Mesh LireFichierOff(string nomFichier)
        {
            if (!File.Exists(nomFichier))
                return null;

            IEnumerable<string> textFichier = File.ReadLines(nomFichier);

            IEnumerator<string> lines = textFichier.GetEnumerator();

            lines.MoveNext();
            lines.MoveNext();

            string line = lines.Current;

            string[] valeurs = line.Split(' ');

            int nSommets = int.Parse(valeurs[0]);
            int nFacettes = int.Parse(valeurs[1]);          

            List<Point> sommets = new List<Point>();
            for(int i = 0; i < nSommets; ++i)
            {
                lines.MoveNext();
                line = lines.Current;
                valeurs = line.Split(' ');

                Point sommet = new Point(
                    double.Parse(valeurs[0], System.Globalization.CultureInfo.InvariantCulture), 
                    double.Parse(valeurs[1], System.Globalization.CultureInfo.InvariantCulture), 
                    double.Parse(valeurs[2], System.Globalization.CultureInfo.InvariantCulture));
                sommets.Add(sommet);
            }

            List<Facette> facettes = new List<Facette>();
            for (int i = 0; i < nFacettes; ++i)
            {
                lines.MoveNext();
                line = lines.Current;
                valeurs = line.Split(' ');

                Facette facette = new Facette(sommets[int.Parse(valeurs[1])], sommets[int.Parse(valeurs[2])], sommets[int.Parse(valeurs[3])]);
                facettes.Add(facette);
            }

            Mesh mesh = new Mesh(sommets, facettes);

            return mesh;
        }
    }
}