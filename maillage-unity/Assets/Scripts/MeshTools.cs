﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshTools
{

    public static List<int> GetConnectedVerticesIndex(Mesh mesh, int verticeIndex)
    {
        List<int> connected = new List<int>();

        for (int i = 0; i < mesh.triangles.Length - 2; i += 3)
        {
            bool found = (mesh.triangles[i] == verticeIndex) || (mesh.triangles[i + 1] == verticeIndex) || (mesh.triangles[i + 2] == verticeIndex);

            if(found)
            {
                if (mesh.triangles[i] != verticeIndex)
                    connected.Add(mesh.triangles[i]);
                if (mesh.triangles[i + 1] != verticeIndex)
                    connected.Add(mesh.triangles[i + 1]);
                if (mesh.triangles[i + 2] != verticeIndex)
                    connected.Add(mesh.triangles[i + 2]);
            }
        }

        return connected;
    }

    public static List<Vector2Int> GetEdges(Mesh mesh)
    {
        List<Vector2Int> edges = new List<Vector2Int>();

        for (int i = 0; i < mesh.triangles.Length - 2; i += 3)
        {
            edges.Add(new Vector2Int(mesh.triangles[i], mesh.triangles[i + 1]));
            edges.Add(new Vector2Int(mesh.triangles[i + 1], mesh.triangles[i + 2]));
            edges.Add(new Vector2Int(mesh.triangles[i + 2], mesh.triangles[i]));
        }

        return edges;
    }

    public static List<Vector3Int> GetTriangles(Mesh mesh)
    {
        List<Vector3Int> triangles = new List<Vector3Int>();

        for (int i = 0; i < mesh.triangles.Length - 2; i += 3)
        {
            triangles.Add(new Vector3Int(mesh.triangles[i], mesh.triangles[i + 1], mesh.triangles[i + 2]));
        }

        return triangles;
    }

    public static List<int> GetTrianglesIndex(Mesh mesh, int verticeIndex)
    {
        List<int> triangles = new List<int>();

        for (int i = 0; i < mesh.triangles.Length - 2; i += 3)
        {
            if (mesh.triangles[i] == verticeIndex || mesh.triangles[i + 1] == verticeIndex || mesh.triangles[i + 2] == verticeIndex)
                triangles.Add(i);
        }

        return triangles;
    }

    public static List<int> GetTrianglesIndex(Mesh mesh, Vector2Int edge)
    {
        List<int> triangles = new List<int>();

        for (int i = 0; i < mesh.triangles.Length - 2; i += 3)
        {
            if (mesh.triangles[i] == edge.x || mesh.triangles[i + 1] == edge.x || mesh.triangles[i + 2] == edge.x)
                if (mesh.triangles[i] == edge.y || mesh.triangles[i + 1] == edge.y || mesh.triangles[i + 2] == edge.y)
                    triangles.Add(i);
        }

        return triangles;
    }

    //public static Mesh CleanTriangles(Mesh mesh)
    //{
    //    List<int> triangles = new List<int>();

    //    for (int i = 0; i < mesh.triangles.Length - 2; i += 3)
    //    {
    //        bool collapsedTriangle = ;
    //        if()
    //    }

    //    return mesh;
    //}

    public static Mesh UpdateMesh(Mesh mesh, Vector3[] vertices, int[] triangles)
    {
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        return mesh;
    }

    public static bool CompareNonOrientedEdges(Vector2Int e1, Vector2Int e2)
    {
        return (e1 == e2) || ((e1.x == e2.y) && (e1.y == e2.x));
    }
}
