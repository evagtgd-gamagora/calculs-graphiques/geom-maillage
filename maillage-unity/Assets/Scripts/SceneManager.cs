﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using geom;
public class SceneManager : MonoBehaviour {

    public Material mat;

    GameObject objPacman;
    GameObject paddock;

    // Use this for initialization
    void Start () {
        Pacman pacman = new Pacman(5, 0.9);

        objPacman = new GameObject();
        objPacman.transform.position = new Vector3(0, 0, 0);
        objPacman.AddComponent<MeshFilter>();
        objPacman.AddComponent<MeshRenderer>();

        objPacman.GetComponent<MeshFilter>().mesh = MeshTranslator.customToUnity(pacman.Mesh(20,8));
        objPacman.GetComponent<MeshRenderer>().material = mat;
        objPacman.name = "Pacman";

        
        List<Point> line = new List<Point>();
        line.Add(new Point(100, -1, 100));
        line.Add(new Point(-100, -1, 100));
        line.Add(new Point(-100, -1, -100));
        line.Add(new Point(100, -1, -100));

        TubeCoude tube = new TubeCoude(10, line);
        paddock = new GameObject();

        paddock.transform.position = new Vector3(0, 0, 0);
        paddock.AddComponent<MeshFilter>();
        paddock.AddComponent<MeshRenderer>();

        paddock.GetComponent<MeshFilter>().mesh = MeshTranslator.customToUnity(tube.Mesh(6));
        paddock.GetComponent<MeshRenderer>().material = mat;
        paddock.name = "Paddock";

    }
	
	// Update is called once per frame
	void Update () {
    }
}
