﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Simplification  {

    public static Mesh CellCollapse(Mesh mesh, float tolerance)
    {
        tolerance = Mathf.Abs(tolerance);
        if (tolerance == 0)
            tolerance = 1000 * float.Epsilon;

        Bounds bounds = mesh.bounds;
        Vector3 center = bounds.center;

        List<Vector3> vertices = new List<Vector3>(mesh.vertices); 
        List<int> triangles = new List<int>(mesh.triangles);

        //Grid vertices gathering
        Dictionary<Vector3Int, List<int>> grid = new Dictionary<Vector3Int, List<int>>();
        Dictionary<int, Vector3Int> reverseGrid = new Dictionary<int, Vector3Int>();

        for (int i = 0; i < vertices.Count; ++i)

        {
            Vector3Int gridPos = new Vector3Int((int) (vertices[i].x / tolerance), (int)(vertices[i].y / tolerance), (int)(vertices[i].z / tolerance));

            if(!grid.ContainsKey(gridPos))
            {
                grid.Add(gridPos, new List<int>());
            }

            if (!grid[gridPos].Contains(i))
                grid[gridPos].Add(i);

            if (reverseGrid.ContainsKey(i))
                reverseGrid.Add(i, gridPos);
        }

        //Vertice merge
        foreach(Vector3Int gridPos in grid.Keys)
        {
            if(grid[gridPos].Count > 1)
            {
                Vector3 merge = new Vector3(0, 0, 0);

                foreach (int v in grid[gridPos])
                {
                    merge += vertices[v];
                }

                merge /= grid[gridPos].Count;

                vertices.Add(merge);
                int mergeIndex = vertices.Count - 1;

                //Face modification
                foreach (int vertice in grid[gridPos])
                {
                    List<int> triangleIndexes = MeshTools.GetTrianglesIndex(mesh, vertice);
                    foreach (int triangleIndex in triangleIndexes)
                    {
                        int i = 0;
                        if (vertice == triangles[triangleIndex])
                        {
                            triangles[triangleIndex] = mergeIndex;
                            ++i;
                        }
                        if (vertice == triangles[triangleIndex + 1])
                        {
                            triangles[triangleIndex] = mergeIndex;
                            ++i;
                        }
                        if (vertice == triangles[triangleIndex + 2])
                        {
                            triangles[triangleIndex] = mergeIndex;
                            ++i;
                        }
                        if (i > 1)

                    }
                }

                //Update mesh
                mesh = MeshTools.UpdateMesh(mesh, vertices.ToArray(), triangles.ToArray());
            }
        }

        return mesh;
    }
}

