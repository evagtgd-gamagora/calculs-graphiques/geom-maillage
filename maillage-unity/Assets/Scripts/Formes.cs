﻿using System;
using System.Collections.Generic;

namespace geom
{
    public class Cube
    {
        double cote;

        public Cube(double cote)
        {
            this.cote = cote;
        }

        public Mesh Mesh()
        {
            List<Point> sommets = new List<Point>();
            List<Facette> facettes = new List<Facette>();

            sommets.Add(new Point(-cote / 2, -cote / 2, -cote / 2));
            sommets.Add(new Point(-cote / 2, -cote / 2, cote / 2));
            sommets.Add(new Point(-cote / 2, cote / 2, -cote / 2));
            sommets.Add(new Point(-cote / 2, cote / 2, cote / 2));
            sommets.Add(new Point(cote / 2, -cote / 2, -cote / 2));
            sommets.Add(new Point(cote / 2, -cote / 2, cote / 2));
            sommets.Add(new Point(cote / 2, cote / 2, -cote / 2));
            sommets.Add(new Point(cote / 2, cote / 2, cote / 2));

            facettes.Add(new Facette(sommets[2], sommets[0], sommets[1]));
            facettes.Add(new Facette(sommets[2], sommets[1], sommets[3]));
            facettes.Add(new Facette(sommets[5], sommets[4], sommets[6]));
            facettes.Add(new Facette(sommets[5], sommets[6], sommets[7]));
            facettes.Add(new Facette(sommets[6], sommets[2], sommets[3]));
            facettes.Add(new Facette(sommets[6], sommets[3], sommets[7]));
            facettes.Add(new Facette(sommets[0], sommets[4], sommets[5]));
            facettes.Add(new Facette(sommets[0], sommets[5], sommets[1]));
            facettes.Add(new Facette(sommets[1], sommets[5], sommets[7]));
            facettes.Add(new Facette(sommets[1], sommets[7], sommets[3]));
            facettes.Add(new Facette(sommets[4], sommets[0], sommets[2]));
            facettes.Add(new Facette(sommets[4], sommets[2], sommets[6]));

            Mesh mesh = new Mesh(sommets, facettes);
            return mesh;
        }

    }

    public class Cylindre
    {
        double rayon;
        double hauteur;

        public Cylindre(double rayon, double hauteur)
        {
            this.rayon = rayon;
            this.hauteur = hauteur;
        }

        public Mesh Mesh(int nMeridiens)
        {
            List<Point> sommets = new List<Point>();
            List<Facette> facettes = new List<Facette>();

            for (int i = 0; i < nMeridiens; ++i)
            {
                sommets.Add(new Point(rayon * Math.Cos(i * 2 * Math.PI / nMeridiens), rayon * Math.Sin(i * 2 * Math.PI / nMeridiens), hauteur / 2));
                sommets.Add(new Point(rayon * Math.Cos(i * 2 * Math.PI / nMeridiens), rayon * Math.Sin(i * 2 * Math.PI / nMeridiens), -hauteur / 2));
            }

            sommets.Add(new Point(0, 0, hauteur / 2));
            sommets.Add(new Point(0, 0, -hauteur / 2));

            for (int i = 0; i < 2 * nMeridiens; i+=2)
            {
                //Cote
                facettes.Add(new Facette(sommets[i], sommets[(i + 1) % (2 * nMeridiens)], sommets[(i + 3) % (2 * nMeridiens)]));
                facettes.Add(new Facette(sommets[i], sommets[(i + 3) % (2 * nMeridiens)], sommets[(i + 2) % (2 * nMeridiens)]));

                //Haut et bas
                facettes.Add(new Facette(sommets[sommets.Count - 2], sommets[i], sommets[(i + 2) % (2 * nMeridiens)]));
                facettes.Add(new Facette(sommets[sommets.Count - 1], sommets[(i + 3) % (2 * nMeridiens)], sommets[(i + 1) % (2 * nMeridiens)]));
            }

            Mesh mesh = new Mesh(sommets, facettes);
            return mesh;
        }

    }

    public class Sphere
    {
        double rayon;

        public Sphere(double rayon)
        {
            this.rayon = rayon;
        }

        public Mesh Mesh(int nMeridiens, int nParalleles)
        {
            List<Point> sommets = new List<Point>();
            List<Facette> facettes = new List<Facette>();

            for (int i = 0; i < nMeridiens; ++i)
            {
                for(int j = 0; j < nParalleles; ++j)
                {
                    double theta = i * 2 * Math.PI / nMeridiens;
                    double phi = (j + 1) * Math.PI / (nParalleles + 1);
                    sommets.Add(new Point(rayon * Math.Sin(phi) * Math.Cos(theta), rayon * Math.Sin(phi) * Math.Sin(theta), rayon * Math.Cos(phi)));
                }
            }

            //Poles Nord et sud
            sommets.Add(new Point(0, 0, rayon));
            sommets.Add(new Point(0, 0, - rayon));


            for (int i = 0; i < nMeridiens ; ++i)
            {
                int select = i * nParalleles;
                int suivant = (i + 1) * nParalleles;

                //Cote
                for (int j = 0; j < nParalleles - 1; ++j)
                {
                    //Cote
                    facettes.Add(new Facette(sommets[select + j], sommets[(select + j + 1) % (nMeridiens * nParalleles)], sommets[(suivant + j + 1) % (nMeridiens * nParalleles)]));
                    facettes.Add(new Facette(sommets[select + j], sommets[(suivant + j + 1) % (nMeridiens * nParalleles)], sommets[(suivant + j) % (nMeridiens * nParalleles)]));
                }

                //Haut et bas
                facettes.Add(new Facette(sommets[sommets.Count - 2], sommets[select], sommets[suivant % (nMeridiens * nParalleles)]));
                facettes.Add(new Facette(sommets[sommets.Count - 1], sommets[(suivant + nParalleles - 1) % (nMeridiens * nParalleles)], sommets[(select + nParalleles - 1) % (nMeridiens * nParalleles)]));
            }

            Mesh mesh = new Mesh(sommets, facettes);
            return mesh;
        }

    }

    public class Cone
    {
        double rayon;
        double hauteur;
        double troncature;
        bool tronc;

        public Cone(double rayon, double hauteur, double troncature)
        {
            this.rayon = rayon;
            this.hauteur = hauteur;
            this.troncature = troncature;
            this.tronc = ((troncature < hauteur) && (troncature > 0));
        }

        public Mesh Mesh(int nMeridiens)
        {
            List<Point> sommets = new List<Point>();
            List<Facette> facettes = new List<Facette>();

            if (tronc)
            {
                double rayonSection = rayon * (1 - troncature / hauteur);

                for (int i = 0; i < nMeridiens; ++i)
                {
                    sommets.Add(new Point(rayonSection * Math.Cos(i * 2 * Math.PI / nMeridiens), rayonSection * Math.Sin(i * 2 * Math.PI / nMeridiens), troncature));
                    sommets.Add(new Point(rayon * Math.Cos(i * 2 * Math.PI / nMeridiens), rayon * Math.Sin(i * 2 * Math.PI / nMeridiens), 0));
                }

                sommets.Add(new Point(0, 0, troncature));
                sommets.Add(new Point(0, 0, 0));
                
                for (int i = 0; i < 2 * nMeridiens; i += 2)
                {
                    //Cote
                    facettes.Add(new Facette(sommets[i], sommets[(i + 1) % (2 * nMeridiens)], sommets[(i + 3) % (2 * nMeridiens)]));
                    facettes.Add(new Facette(sommets[i], sommets[(i + 3) % (2 * nMeridiens)], sommets[(i + 2) % (2 * nMeridiens)]));

                    //Haut et bas
                    facettes.Add(new Facette(sommets[sommets.Count - 2], sommets[i], sommets[(i + 2) % (2 * nMeridiens)]));
                    facettes.Add(new Facette(sommets[sommets.Count - 1], sommets[(i + 3) % (2 * nMeridiens)], sommets[(i + 1) % (2 * nMeridiens)]));
                }
            }
            else
            {

                for (int i = 0; i < nMeridiens; ++i)
                {
                    sommets.Add(new Point(rayon * Math.Cos(i * 2 * Math.PI / nMeridiens), rayon * Math.Sin(i * 2 * Math.PI / nMeridiens), 0));
                }

                sommets.Add(new Point(0, 0, hauteur));
                sommets.Add(new Point(0, 0, 0));

                for (int i = 0; i < nMeridiens; ++i)
                {
                    //Cote
                    facettes.Add(new Facette(sommets[sommets.Count - 2], sommets[i % nMeridiens], sommets[(i + 1) % nMeridiens]));

                    //Bas
                    facettes.Add(new Facette(sommets[sommets.Count - 1], sommets[(i + 1) % nMeridiens], sommets[i %  nMeridiens]));
                }
            }
            

            Mesh mesh = new Mesh(sommets, facettes);
            return mesh;
        }

    }

    public class Pacman
    {
        double rayon;
        double proportion;

        public Pacman(double rayon, double proportion)
        {
            this.rayon = rayon;
            this.proportion = Math.Min(1, Math.Max(0, proportion));
        }

        public Mesh Mesh(int nMeridiens, int nParalleles)
        {
            List<Point> sommets = new List<Point>();
            List<Facette> facettes = new List<Facette>();

            for (int i = 0; i < nMeridiens; ++i)
            {
                for (int j = 0; j < nParalleles; ++j)
                {
                    double theta = i * 2 * Math.PI * proportion/ nMeridiens;
                    double phi = (j + 1) * Math.PI / (nParalleles + 1);
                    sommets.Add(new Point(rayon * Math.Sin(phi) * Math.Cos(theta), rayon * Math.Sin(phi) * Math.Sin(theta), rayon * Math.Cos(phi)));
                }
            }
            
            //Interieur
            for (int i = 0; i < nParalleles; ++i)
            {
                sommets.Add(new Point(0, 0, rayon * (1 - 2.0d * (i + 1.0d) / (nParalleles + 1.0d) )));
            }

            //Poles Nord et sud
            sommets.Add(new Point(0, 0, rayon));
            sommets.Add(new Point(0, 0, -rayon));

            for (int i = 0; i < nMeridiens - 1; ++i)
            {
                int select = i * nParalleles;
                int suivant = (i + 1) * nParalleles;

                //Cote
                for (int j = 0; j < nParalleles - 1; ++j)
                {
                    //Cote
                    facettes.Add(new Facette(sommets[select + j], sommets[(select + j + 1) % (nMeridiens * nParalleles)], sommets[(suivant + j + 1) % (nMeridiens * nParalleles)]));
                    facettes.Add(new Facette(sommets[select + j], sommets[(suivant + j + 1) % (nMeridiens * nParalleles)], sommets[(suivant + j) % (nMeridiens * nParalleles)]));
                }

                //Haut et bas
                facettes.Add(new Facette(sommets[sommets.Count - 2], sommets[select], sommets[suivant % (nMeridiens * nParalleles)]));
                facettes.Add(new Facette(sommets[sommets.Count - 1], sommets[(suivant + nParalleles - 1) % (nMeridiens * nParalleles)], sommets[(select + nParalleles - 1) % (nMeridiens * nParalleles)]));
            }

            int nSurface = nMeridiens * nParalleles;

            //Interieur
            for (int i = 0; i < nParalleles - 1; ++i)
            {
                facettes.Add(new Facette(sommets[nSurface + i], sommets[nSurface + i + 1], sommets[i]));
                facettes.Add(new Facette(sommets[nSurface + i + 1], sommets[i + 1], sommets[i]));

                int j = nSurface - nParalleles + i;
                facettes.Add(new Facette(sommets[nSurface + i], sommets[j], sommets[nSurface + i + 1]));
                facettes.Add(new Facette(sommets[nSurface + i + 1], sommets[j], sommets[j + 1]));
            }

            //Interieur poles nord et sud 
            facettes.Add(new Facette(sommets[sommets.Count - 2], sommets[nSurface], sommets[0]));
            facettes.Add(new Facette(sommets[sommets.Count - 2], sommets[nSurface - nParalleles], sommets[nSurface]));
            int offset = nParalleles - 1;
            facettes.Add(new Facette(sommets[sommets.Count - 1], sommets[offset], sommets[nSurface + offset]));
            facettes.Add(new Facette(sommets[sommets.Count - 1], sommets[nSurface + offset], sommets[nSurface - 1]));



            Mesh mesh = new Mesh(sommets, facettes);
            return mesh;
        }
    }

    public class LigneCylindrique
    {
        double rayon;
        List<Point> ligne;

        public LigneCylindrique(double rayon, List<Point> ligne)
        {
            this.rayon = rayon;
            this.ligne = ligne;
        }

        public Mesh Mesh(int nMeridiens)
        {
            List<Point> sommets = new List<Point>();
            List<Facette> facettes = new List<Facette>();

            int nLigne = ligne.Count;

            for (int i = 0; i < nMeridiens; ++i)
            {
                for(int j = 0; j < nLigne; ++j)
                {
                    sommets.Add(
                        new Point(
                            ligne[j].x + rayon * Math.Cos(i * 2 * Math.PI / nMeridiens),
                            ligne[j].y + rayon * Math.Sin(i * 2 * Math.PI / nMeridiens),
                            ligne[j].z
                            )
                    );
                }
            }

            sommets.Add(ligne[0]);
            sommets.Add(ligne[nLigne - 1]);

            int nSurface = nMeridiens * nLigne;

            for (int i = 0; i < nMeridiens; ++i)
            {
                int a = i * nLigne;
                for (int j = 0; j < nLigne - 1; ++j)
                {
                    int b = a + j;
                    //Cote
                    facettes.Add(new Facette(sommets[b], sommets[(b + nLigne) % nSurface], sommets[(b + 1) % nSurface]));
                    facettes.Add(new Facette(sommets[(b + nLigne) % nSurface], sommets[(b + nLigne + 1) % nSurface], sommets[(b + 1) % nSurface]));
                }

                //Haut et bas
                facettes.Add(new Facette(sommets[sommets.Count - 2], sommets[(a + nLigne) % nSurface], sommets[a]));
                facettes.Add(new Facette(sommets[sommets.Count - 1], sommets[(a + nLigne - 1) % nSurface], sommets[(a + 2 * nLigne - 1) % nSurface]));
            }

            Mesh mesh = new Mesh(sommets, facettes);
            return mesh;
        }

    }

    public class TubeCoude
    {
        double rayon;
        List<Point> ligne;

        public TubeCoude(double rayon, List<Point> ligne)
        {
            this.rayon = rayon;
            this.ligne = ligne;
        }

        public Mesh Mesh(int nMeridiens)
        {
            List<Point> sommets = new List<Point>();
            List<Facette> facettes = new List<Facette>();

            int nLigne = ligne.Count;

            //Calcul vecteurs
            List<Point> normales = new List<Point>();
            List<Point> binormales = new List<Point>();

            //Calcul des plans sur la ligne
            for (int i = 0; i < nLigne; ++i)
            {
                int debut = Math.Max(0, i - 1);
                int fin = Math.Min(nLigne - 1, i + 1);
                Point tangente = new Point(
                    ligne[fin].x - ligne[debut].x,
                    ligne[fin].y - ligne[debut].y,
                    ligne[fin].z - ligne[debut].z
                );

                double taille = Math.Sqrt(Math.Pow(tangente.x, 2) + Math.Pow(tangente.y, 2) + Math.Pow(tangente.z, 2));

                tangente.x /= taille;
                tangente.y /= taille;
                tangente.z /= taille;

                Point normale = new Point(
                    tangente.z,
                    tangente.z,
                    -(tangente.x + tangente.y)
                );

                taille = Math.Sqrt(Math.Pow(normale.x, 2) + Math.Pow(normale.y, 2) + Math.Pow(normale.z, 2));

                normale.x /= taille;
                normale.y /= taille;
                normale.z /= taille;

                Point binormale = new Point(
                    tangente.y * normale.z - tangente.z * normale.y,
                    tangente.z * normale.x - tangente.x * normale.z,
                    tangente.x * normale.y - tangente.y * normale.x
                );

                taille = Math.Sqrt(Math.Pow(binormale.x, 2) + Math.Pow(binormale.y, 2) + Math.Pow(binormale.z, 2));

                binormale.x /= taille;
                binormale.y /= taille;
                binormale.z /= taille;

                //Verifie que les vecteurs du plan sont à peu près
                normales.Add(normale);
                binormales.Add(binormale);
            }

            //Calcul de sommets
            for (int i = 0; i < nMeridiens; ++i)
            {
                for (int j = 0; j < nLigne; ++j)
                {
                    sommets.Add(
                        new Point(
                            ligne[j].x + rayon * normales[j].x * Math.Cos(i * 2 * Math.PI / nMeridiens) + rayon * binormales[j].x * Math.Sin(i * 2 * Math.PI / nMeridiens),
                            ligne[j].y + rayon * normales[j].y * Math.Cos(i * 2 * Math.PI / nMeridiens) + rayon * binormales[j].y * Math.Sin(i * 2 * Math.PI / nMeridiens),
                            ligne[j].z + rayon * normales[j].z * Math.Cos(i * 2 * Math.PI / nMeridiens) + rayon * binormales[j].z * Math.Sin(i * 2 * Math.PI / nMeridiens)
                        )
                    );
                }
            }

            sommets.Add(ligne[0]);
            sommets.Add(ligne[nLigne - 1]);

            int nSurface = nMeridiens * nLigne;

            //Calcul de facettes
            for (int i = 0; i < nMeridiens; ++i)
            {
                int a = i * nLigne;
                for (int j = 0; j < nLigne - 1; ++j)
                {
                    int b = a + j;
                    //Cote
                    facettes.Add(new Facette(sommets[b], sommets[(b + nLigne) % nSurface], sommets[(b + 1) % nSurface]));
                    facettes.Add(new Facette(sommets[(b + nLigne) % nSurface], sommets[(b + nLigne + 1) % nSurface], sommets[(b + 1) % nSurface]));
                }

                //Haut et bas
                facettes.Add(new Facette(sommets[sommets.Count - 2], sommets[(a + nLigne) % nSurface], sommets[a]));
                facettes.Add(new Facette(sommets[sommets.Count - 1], sommets[(a + nLigne - 1) % nSurface], sommets[(a + 2 * nLigne - 1) % nSurface]));
            }

            Mesh mesh = new Mesh(sommets, facettes);
            return mesh;
        }

    }
}
