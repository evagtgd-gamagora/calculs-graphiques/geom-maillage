﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using geom;

public static class MeshTranslator
{
    public static UnityEngine.Mesh customToUnity(geom.Mesh customMesh)
    {
        int nVertices = customMesh.sommets.Count;
        int nTriangles = customMesh.facettes.Count;

        Vector3[] vertices = new Vector3[nVertices];
        int[] triangles = new int[3*nTriangles];

        for (int i = 0; i < nVertices; ++i)
        {
            geom.Point point = customMesh.sommets[i];
            vertices[i] = new Vector3((float)point.x, (float)point.y, (float)point.z);
        }

        for (int i = 0; i < nTriangles; ++i)
        {
            geom.Facette facette = customMesh.facettes[i];
            triangles[3 * i] = customMesh.sommets.IndexOf(facette.p1);
            triangles[3 * i + 1] = customMesh.sommets.IndexOf(facette.p2);
            triangles[3 * i + 2] = customMesh.sommets.IndexOf(facette.p3);
        }


        UnityEngine.Mesh mesh = new UnityEngine.Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;

        return mesh;
    }
}
