﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using geom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace geom.Tests
{
    [TestClass()]
    public class AreteTests
    {
        [TestMethod()]
        public void EqualsTest()
        {
            Point p1 = new Point(2, 3, 4);
            Point p2 = new Point(-4, 5, 23);

            Arete a1 = new Arete(p1, p2);
            Arete a2 = new Arete(p2, p1);
            Arete a3 = new Arete(p1, p1);
            Arete a4 = new Arete(p1, p2);

            Assert.IsTrue(a1.Equals(a1));
            Assert.IsTrue(a1.Equals(a2));
            Assert.IsTrue(a2.Equals(a1));
            Assert.IsTrue(a4.Equals(a1));

            Assert.IsFalse(a1.Equals(a3));
            Assert.IsFalse(a3.Equals(a1));
            Assert.IsFalse(a2.Equals(a3));

            Assert.IsFalse(a1 == a2);
            Assert.IsFalse(a1 == a4);
        }

        [TestMethod()]
        public void GetHashCodeTest()
        {
            Point p1 = new Point(2, 3, 4);
            Point p2 = new Point(-4, 5, 23);

            Arete a1 = new Arete(p1, p2);
            Arete a2 = new Arete(p2, p1);
            Arete a3 = new Arete(p1, p1);

            List<Arete> aretes = new List<Arete>();
            aretes.Add(a1);

            Assert.IsTrue(a1.GetHashCode() == a2.GetHashCode());
            Assert.IsFalse(a1.GetHashCode() == a3.GetHashCode());

            Assert.IsTrue(aretes.Contains(a1));
            Assert.IsTrue(aretes.Contains(a2));
        }
    }
}