﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using geom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace geom.Tests
{
    [TestClass()]
    public class PointTests
    {
        [TestMethod()]
        public void NormaliserTest()
        {
            Point p = new Point(5, 5, 5);
            p.Normaliser();
            Assert.AreEqual(p.Longueur(), 1);
            Assert.AreEqual(p.LongueurAuCarre(), 1);
        }

        [TestMethod()]
        public void EqualsTest()
        {
            Point p1 = new Point(-5, -5, -5);
            Point p2 = new Point(-5, -5, -5);
            Point p3 = new Point(5, 5, 5);

            Assert.IsTrue(p1.Equals(p2));
            Assert.IsTrue(p2.Equals(p1));
            Assert.IsTrue(p1.Equals(p1));

            Assert.IsFalse(p1.Equals(p3));
            Assert.IsFalse(p3.Equals(p1));

            Assert.IsFalse(p1 == p2);
        }
    }
}